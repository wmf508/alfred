package mongodb

import "go.mongodb.org/mongo-driver/bson/primitive"

type Config struct {
	Username string // 数据库用户名
	Password string // 数据库密码
	Address  string // url主机名, 如：127.0.0.1 或者 www.mongodb.com
	Port     int    // 端口，如：27017
}

type CommonField struct {
	// mongodb id
	BsonId *primitive.ObjectID `json:"bsid,omitempty" bson:"_id,omitempty"`
}

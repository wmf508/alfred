package mongodb

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetClient(t *testing.T) {
	config := Config{}

	cli, err := GetClient(config)

	require.NoError(t, err)
	require.NotEmpty(t, cli)

	t.Log("create mongodb client success.")
}

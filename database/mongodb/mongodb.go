package mongodb

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	// default username for database connection
	defaultUsername = "admin"
	// default password for database connection
	defaultPassword = "12345"
	// default host address for database connection
	defaultHost = "127.0.0.1"
	// default port for database connection
	defaultPort = 27017
)

// 检查传入的配置文件，为没有配置的字段设置初始值
func configGenerator(originalConfig Config) Config {
	var validatedConfig Config

	if originalConfig.Username != "" {
		validatedConfig.Username = originalConfig.Username
	} else {
		validatedConfig.Username = defaultUsername
	}
	if originalConfig.Password != "" {
		validatedConfig.Password = originalConfig.Password
	} else {
		validatedConfig.Password = defaultPassword
	}
	if originalConfig.Address != "" {
		validatedConfig.Address = originalConfig.Address
	} else {
		validatedConfig.Address = defaultHost
	}
	if originalConfig.Port != 0 {
		validatedConfig.Port = originalConfig.Port
	} else {
		validatedConfig.Port = defaultPort
	}

	return validatedConfig
}

// initialize mongodb
func GetClient(originalConfig Config) (*mongo.Client, error) {

	config := configGenerator(originalConfig)

	uri := fmt.Sprintf("mongodb://%s:%s@%s:%d", config.Username, config.Password, config.Address, config.Port)

	log.Printf("connect to mongodb: mongodb://***:***@%s:%d", config.Address, config.Port)

	// connect to mongodb
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
	if err != nil {
		log.Printf("create mongodb client failed:%s", err.Error())
		return nil, err
	}

	// 测试连接是否成功
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Printf("connect to mongodb failed:%s\n", err.Error())
		return nil, err
	}

	log.Println("connect to mongodb success!")

	return client, nil
}

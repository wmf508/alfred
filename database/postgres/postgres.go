package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type Config struct {
	Username string // 数据库用户名
	Password string // 数据库密码
	Address  string // url主机名, 如：127.0.0.1 或者localhost
	Port     int    // 端口，如：5432
	Database string // 访问的数据库名
	SslMode  string //是否开启ssl模式访问, 如：disable(不开启)
}

func GetClient(config Config) (*sql.DB, error) {
	dbSource := fmt.Sprintf(
		"postgresql://%s:%s@%s:%d/%s?sslmode=%s",
		config.Username,
		config.Password,
		config.Address,
		config.Port,
		config.Database,
		config.SslMode,
	)
	cli, err := sql.Open("postgres", dbSource)

	if err != nil {
		return nil, fmt.Errorf("cannot connect to postgres: %v", err)
	}

	return cli, nil
}

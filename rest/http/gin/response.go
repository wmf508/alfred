package gin

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/wmf508/alfred/errorcode"
)

type Response struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func BasicResp(c *gin.Context, statusCode int, code string, data interface{}, message string) {
	response := Response{
		Code:    code,
		Message: message,
		Data:    data,
	}

	c.JSON(statusCode, response)
}

func BadRequestErrorResp(c *gin.Context, code string, message string) {
	BasicResp(c, http.StatusBadRequest, code, nil, fmt.Sprintf("Bad Request: %s", message))
}

func InternalServerErrorResp(c *gin.Context, code string, message string) {
	BasicResp(c, http.StatusInternalServerError, code, nil, fmt.Sprintf("Internal Server Error: %s", message))
}

func InvalidErrCodeResp(c *gin.Context, message string) {
	InternalServerErrorResp(c, errorcode.InvalidErrCode, message)
}

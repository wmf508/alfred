package log

import (
	"log"
	"os"
)

type Logger = *log.Logger

func GetLogger() Logger {
	log := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)
	return log
}

package errormap

type ErrorMap map[string]*ErrorDesc

type ErrorDesc struct {
	ErrorCode string
	LogMsg    string
	UserMsg   string
}

var ProjectMap = map[string]ErrorMap{
	"E000020": E000020,
}

func (desc *ErrorDesc) GetLogMsgWithCode() string {
	return desc.ErrorCode + ":" + desc.LogMsg
}

func (desc *ErrorDesc) GetUserMsgWithCode() string {
	return desc.ErrorCode + ":" + desc.UserMsg
}

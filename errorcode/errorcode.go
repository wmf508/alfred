package errorcode

import (
	"gitlab.com/wmf508/alfred/errorcode/errormap"
)

// 该函数只能0级项目使用
func (errCode *ErrorCode) GetDesc(code string) (*errormap.ErrorDesc, error) {
	return BasicGetDesc(code, errormap.ProjectMap)
}

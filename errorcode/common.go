package errorcode

import (
	"fmt"

	"gitlab.com/wmf508/alfred/errorcode/errormap"
)

type ErrorCode struct {
}

const (
	InvalidErrCode = "E000000000"
)

func GetErrorCode() *ErrorCode {
	return &ErrorCode{}
}

// 提供给各alfredy封装用的底层方法
func BasicGetDesc(code string, errMap map[string]errormap.ErrorMap) (*errormap.ErrorDesc, error) {

	err := ValidateCode(code)

	if err != nil {
		return nil, fmt.Errorf("invalid code %s: %v", code, err)
	}

	// 项目名为code前6位，再加上E前缀
	projectCode := code[0:7]

	errCode, ok := errMap[projectCode]
	if !ok {
		return nil, fmt.Errorf("project code %v is not exist", projectCode)
	}

	desc, ok := errCode[code]

	if !ok {
		return nil, fmt.Errorf("error code %v is not exist", code)
	}

	return desc, nil
}

func ValidateCode(code string) error {
	// TODO: 验证code是否合规
	return nil
}

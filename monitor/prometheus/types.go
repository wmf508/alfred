package prometheus

type Config struct {
	Address  string // prometheus server服务地址中的主机名, 如: 192.168.1.2 或者 example.com
	Port     int    // 端口
	Protocol string // 协议：http或者https
}

package prometheus

import (
	"fmt"
	"log"

	"github.com/prometheus/client_golang/api"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
)

var (
	defaultHostname = "127.0.0.1"
	defaultPort     = 9090
	defaultProtocol = "http"
)

// 检查传入的配置文件，为没有配置的字段设置初始值
func configGenerator(originalConfig Config) Config {
	config := Config{
		Address:  originalConfig.Address,
		Port:     originalConfig.Port,
		Protocol: originalConfig.Protocol,
	}

	// 没有值的选项设置默认值
	if originalConfig.Address == "" {
		config.Address = defaultHostname
	}
	if originalConfig.Port == 0 {
		config.Port = defaultPort
	}
	if originalConfig.Protocol == "" {
		config.Protocol = defaultProtocol
	}

	return config
}

// initialize prometheus api
func GetApi(originalConfig Config) (v1.API, error) {
	config := configGenerator(originalConfig)

	addr := fmt.Sprintf("%s://%s:%v", config.Protocol, config.Address, config.Port)

	log.Printf("prometheus client address: %s", addr)

	client, err := api.NewClient(api.Config{
		Address: addr,
	})

	if err != nil {
		log.Printf("Error creating prometheus client: %s", err.Error())
		return nil, err
	}

	log.Println("create prometheus client success!")

	v1api := v1.NewAPI(client)

	return v1api, nil
}

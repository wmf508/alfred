package prometheus

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetApi(t *testing.T) {
	config := Config{}

	api, err := GetApi(config)

	require.NoError(t, err)
	require.NotEmpty(t, api)

	t.Log(" create prometheus api success.")
}
